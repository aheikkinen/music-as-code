
aPart1a = [
  "Gb5|B2 s=3", "D5|Ab2 s=3", "D5 s=0.75", "E5 s=0.75",
  "F5|Db3 s=2", "E5|Db3 s=2", "D5|Db3",
  "Db5|Gb2 s=2", "D5|Gb2 s=2", "E5|Gb2",
  "Gb5|B2 s=3"
]
aPart1b = [
  "B5|Ab2 s=3", "B4|Ab2 s=2", "Db5|Ab2",
  "D5|Db3 s=2", "E5|Db3 s=2", "D5|Db3",
  "Db5|Gb3 s=2", "A5|Gb3 s=2", "G5|Gb3"
]
aPart1c = [
  "B5|Ab2 s=3", "B5|Ab2",
  "Db6|Ab2", "D6|Db3 s=2", "G5|Db3 s=2", "Gb5|Db3",
  "F5|Gb2 s=2", "D6|Gb2 s=2", "E6|Gb2"
]

aPart2 = [
  "B3", "D4", "E4", "F4", "Gb4 s=5",
  "B4", "D5", "A5",
  "B4|D5|Ab5 s=5",
  "D5|Gb5 s=2",
  "Gb5|E5",
  "D5|Gb5 s=6",
  "B4", "D5", "E5", "F5", "Gb5 s=5"
]

with_fx :reverb, room: 0.7, mix: 0.7 do
  use_bpm 300
  live_loop :base do
    use_synth :pretty_bell
    playKeys [aPart1a, aPart1b, aPart1a, aPart1c]
    sleep 1
    playKeys [aPart2]
    sleep 0.5
  end
end

define :playKeys do |aKeySets, nAmp=1|
  aKeySets.each do |aKeySet|
    aKeySet.each do |sKeys|
      nGlobalSustain, nGlobalRelease, nGlobalAttack = nil
      aPlayKeys = []
      sKeys.split("|").each do |sKeyInfo|
        rSustain = sKeyInfo.match(/s[:=]([\d\.]+)/)
        rRelease = sKeyInfo.match(/r[:=]([\d\.]+)/)
        rAttack = sKeyInfo.match(/a[:=]([\d\.]+)/)
        rPause = sKeyInfo.match(/p[:=]([\d\.]+)/)
        nSustain = (rSustain and rSustain.length > 0) ? rSustain[1].to_f : nSustain
        nRelease = (rRelease and rRelease.length > 0) ? rRelease[1].to_f : nRelease
        nAttack = (rAttack and rAttack.length > 0) ? rAttack[1].to_f : nAttack
        nPause = (rPause and rPause.length > 0) ? rPause[1].to_f : nPause
        nGlobalSustain = nGlobalSustain.nil? ? nSustain : nil
        nGlobalRelease = nGlobalRelease.nil? ? nRelease : nil
        nGlobalAttack = nGlobalAttack.nil? ? nAttack : nil
        aPlayKeys << {
          :key => sKeyInfo.split(" ")[0],
          :sustain => nSustain,
          :release => nRelease,
          :attack => nAttack,
          :pause => nPause
        }
      end
      aPlayKeys.each do |rKey|
        nSustain = rKey[:sustain].nil? ? (nGlobalSustain.nil? ? 1 : nGlobalSustain) : rKey[:sustain]
        if nSustain.nil? then
          nSustain, nGlobalSustain = 1
        end
        sleep rKey[:pause].nil? ? 0 : rKey[:pause]
        play rKey[:key],
          sustain: nSustain,
          release: (rKey[:release].nil? ? (nGlobalRelease.nil? ? nSustain : nGlobalRelease) : rKey[:release]),
          attack: (rKey[:attack].nil? ? (nGlobalAttack.nil? ? 0 : nGlobalAttack) : rKey[:attack]),
          amp: nAmp
      end
      sleep nGlobalSustain.nil? ? 1 : nGlobalSustain
    end
  end
end