# Welcome to Sonic Pi

use_bpm 50

in_thread do
  live_loop :amen_break do
    p = [0.125, 0.25, 0.5].choose
    with_fx :slicer, phase: p, wave: 0 do
      sample :loop_breakbeat, beat_stretch: 2, rate: 1, amp: 2
    end
    sleep 2
  end
end