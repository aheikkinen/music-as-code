
with_fx :echo do
  live_loop :guitar do
    sample :guit_harmonics, onset: pick, amp: 2, sustain: 0, release: 0.7
    sleep 0.25
  end
end

with_fx :bitcrusher, room: 0.7 do
  live_loop :glitch do
    sample :glitch_perc2
    sleep 0.75
  end
end

with_fx :reverb, room: 0.7 do
  live_loop :drums do
    sample :drum_bass_hard, amp: 2
    sleep 0.5
    rrand(2,3).times do
      sample :drum_snare_soft, amp: 2
      sleep 0.25
    end
    sample :drum_cymbal_open, amp: 0.5
    sleep 0.5
  end
end