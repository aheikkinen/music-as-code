
aPart1 = [
  "D5 s=2|F4 p=1", "A4", "E5 s=3|E4 p=2", "A4 s=2",
  "F5 s=2|F4 p=1", "C5", "G5 s=3|G4 p=2", "D5 s=2"
]

aPart2 = [
  "C5 s=0.75", "D5 s=0.75", "F5 s=0.75", "G5 s=0.75", "F5 s=0.75", "D5 s=0.75",
  "C5 s=0.75", "D5 s=0.75", "F5 s=0.75", "D5 s=0.75", "C5 s=0.75",
  "A4", "A4", "A4", "A4 s=4",
]

aPart3a = [
  "A4", "C5",
  "D4|F4|A4|D5 s=5",
  "F5", "E5 s=1.5", "D5 s=0.50"
]

aPart3b1 = [
  "E4|G4|A4|E5 s=5"
]

aPart3b2 = [
  "E4|G4|C4|E5 s=5"
]

aPart3c = [
  "G5", "F5 s=1.5", "E5 s=0.5",
  "F4|A4|C5|F5 s=5",
  "A5", "G5", "F5",
  "G4|B4|D5|G5 s=3",
  "D5|G5 s=2",
  "G5", "G5", "A5",
  "Bb4|D5|F5|Bb5 s=5",
  "F5", "G5", "Bb5",
  "C5|E5|A5 s=5",
  "G5", "G5", "F5",
  "D5|G5|A5 s=5"
]

aPart3d = [
  "G5", "Bb5",
  "E5|G5|C6 s=5"
]

aPart4a = [
  "F4|D5|A5 s=3",
  "G4|E5|B5 s=2",
  "G5",
]

aPart4b1 = [
  "A4|F5|C5 s=4",
  "A4|F5|C5 s=0.5",
  "A4|F5|C5 s=1",
  "A4|F5|C5 s=0.5",
  "A4|F5|C5 s=1",
  "A4|F5|C5 s=0.5",
  "A4|F5|C5 s=0.5",
]

aPart4b2 = [
  "A4|F5|C5 s=4",
  "A4|F5|C5 s=0.5",
  "A4|F5|C5 s=1",
  "A4|F5|C5 s=0.5",
  "A4|F5|C5 s=1",
  "D5",
  "A4|F5|C5 s=0.5",
  "A4|F5|C5 s=0.5",
  "G5",
  "D5|G5|C6 s=8"
]

aPart4c = [
  "F4|D5|A5 s=3",
  "G4|E5|B5 s=3",
  "C5"
]

with_fx :rbpf, room: 0.7 do
  use_synth :tech_saws
  use_bpm 300
  in_thread do
    playKeys [aPart1, aPart1, aPart1]
    playKeys [aPart2]
    playKeys [aPart3a, aPart3b1, aPart3c]
    sleep 2
    playKeys [aPart3a, aPart3b2, aPart3c, aPart3d]
    playKeys [aPart4a, aPart4b1, aPart4c, aPart4b1, aPart4a, aPart4b2]
    playKeys [aPart1, aPart1, aPart1]
    playKeys [aPart2]
    sleep 1
  end
end

define :playKeys do |aKeySets, nAmp=1|
  aKeySets.each do |aKeySet|
    aKeySet.each do |sKeys|
      nGlobalSustain, nGlobalRelease, nGlobalAttack = nil
      aPlayKeys = []
      sKeys.split("|").each do |sKeyInfo|
        rSustain = sKeyInfo.match(/s[:=]([\d\.]+)/)
        rRelease = sKeyInfo.match(/r[:=]([\d\.]+)/)
        rAttack = sKeyInfo.match(/a[:=]([\d\.]+)/)
        rPause = sKeyInfo.match(/p[:=]([\d\.]+)/)
        nSustain = (rSustain and rSustain.length > 0) ? rSustain[1].to_f : nSustain
        nRelease = (rRelease and rRelease.length > 0) ? rRelease[1].to_f : nRelease
        nAttack = (rAttack and rAttack.length > 0) ? rAttack[1].to_f : nAttack
        nPause = (rPause and rPause.length > 0) ? rPause[1].to_f : nPause
        nGlobalSustain = nGlobalSustain.nil? ? nSustain : nil
        nGlobalRelease = nGlobalRelease.nil? ? nRelease : nil
        nGlobalAttack = nGlobalAttack.nil? ? nAttack : nil
        aPlayKeys << {
          :key => sKeyInfo.split(" ")[0],
          :sustain => nSustain,
          :release => nRelease,
          :attack => nAttack,
          :pause => nPause
        }
      end
      aPlayKeys.each do |rKey|
        nSustain = rKey[:sustain].nil? ? (nGlobalSustain.nil? ? 1 : nGlobalSustain) : rKey[:sustain]
        if nSustain.nil? then
          nSustain, nGlobalSustain = 1
        end
        sleep rKey[:pause].nil? ? 0 : rKey[:pause]
        play rKey[:key],
          sustain: nSustain,
          release: (rKey[:release].nil? ? (nGlobalRelease.nil? ? nSustain : nGlobalRelease) : rKey[:release]),
          attack: (rKey[:attack].nil? ? (nGlobalAttack.nil? ? 0 : nGlobalAttack) : rKey[:attack]),
          amp: nAmp
      end
      sleep nGlobalSustain.nil? ? 1 : nGlobalSustain
    end
  end
end