
aPart1a = ["B1", "B1", "E2",  "Gb2 s=2", "B1", "E2",  "Gb2"]
aPart1b = ["A1", "A1", "D2",  "E2 s=2",  "A1", "D2",  "E2" ]
aPart1c = ["G1", "G1", "C2",  "D2 s=2",  "G1", "C2",  "D2" ]
aPart1d = ["F1", "F1", "Bb1", "C2 s=2",  "F1", "Bb1", "C2" ]
aPart1e = ["C2", "C2", "F2",  "G2 s=2",  "C2", "F2",  "G2" ]
aPart1f = ["G1", "G1", "Gb2", "D2 s=2",  "G1", "A1",  "B1" ]

aPart2 = [
  "B4",      "Bb4",    "Db4",    "B4 s=2", "Bb4",     "Db4 s=2",
  "B4",      "Bb4",    "Db4",    "B4 s=2", "Bb4",     "Db5 s=2",
  "B4",      "A4",     "Ab4",    "E4 s=3", "B3 s=2",  "A4",
  "Ab4 s=2", "B4 s=2", "A4",     "Ab4",    "E4",      "B4",
  "Bb4",     "Db4",    "B4 s=2", "Bb4",    "Db4 s=2", "B4",
  "Bb4",     "Db4",    "B4 s=2", "Bb4",    "Db5 s=2", "B4",
  "A4",      "Ab4",    "E4 s=3", "B3 s=2", "A4",      "Ab4 s=2",
  "B4 s=5"
]
aPart3 = [
  "D4|G4 s=3", "Gb4 s=5",
  "C4|F4 s=3", "E4 s=3",
  "C4 s=2",    "D4|G4 s=3",
  "Gb4 s=6"
]
aPart4 = [
  "Eb4|G4",  "Eb4|G4 p=1",  "Eb4|G4 p=1",  "Eb4|G4 p=1",
  "Eb4|Gb4", "Eb4|Gb4 p=1", "Eb4|Gb4 p=1", "Eb4|Gb4 p=1",
  "Db4|Gb4", "Db4|Gb4 p=1", "Db4|Gb4 p=1", "Db4|Gb4 p=1",
  "B3|E4",   "B3|E4 p=1",   "B3|E4 p=1",   "B3|E4 p=1",
  "Eb4|G4",  "Eb4|G4 p=1",  "Eb4|G4 p=1",  "Eb4|G4 p=1",
  "Eb4|Gb4", "Eb4|Gb4 p=1", "Eb4|Gb4 p=1", "Eb4|Gb4 p=1",
  "Db4|Gb4", "Db4|Gb4 p=1", "Db4|Gb4 p=1", "Db4|Gb4 p=1",
  "B3|E4",   "B3|E4 p=1",   "B3|E4 p=1",   "B3|E4 p=1",
]

with_fx :rbpf, amp: 1, room: 0.7, mix: 0.7 do
  use_bpm 320
  live_loop :base do
    use_synth :dpulse
    
    sample :vinyl_rewind
    
    # Phase 1
    playKeys [aPart1a*2, aPart1b*2]
    cue :startPianoPhase2
    cue :startDrumPhase1
    playKeys [aPart1a*2, aPart1b*2]*2
    playKeys [aPart1c, aPart1d, aPart1c]
    
    2.times do
      sample :vinyl_scratch
      sleep 1
      sample :vinyl_hiss
    end
    sleep 6
    
    # Phase 2
    playKeys [aPart1a*2, aPart1b*2]
    cue :startPianoPhase2
    playKeys [aPart1a*2, aPart1b*2]*2
    playKeys [aPart1c, aPart1d, aPart1c]
    
    sample :vinyl_backspin
    sleep 8
    
    # Phase 3
    cue :startPianoPhase3
    playKeys [aPart1e, aPart1a, aPart1b, aPart1f]*2
    
    # Phase 4
    playKeys [aPart1a*2, aPart1b*2]
    cue :startPianoPhase2
    playKeys [aPart1a*2, aPart1b*2]*2
    playKeys [aPart1c, aPart1d, aPart1c]
    
    sleep 8
  end
end
with_fx :reverb, room: 0.7, mix: 0.7 do
  use_bpm 320
  live_loop :pianoPhase2 do
    sync :startPianoPhase2
    use_synth :blade
    playKeys [aPart2], 1
    playKeys [aPart3], 1
    sleep 1
  end
  live_loop :pianoPhase3 do
    sync :startPianoPhase3
    use_synth :blade
    playKeys [aPart4], 1
    sleep 1
  end
end

with_fx :bitcrusher, room: 0.7, mix: 0.7 do
  in_thread do
    live_loop :vinyl, amp: 0.3 do
      sample :elec_cymbal, amp: 0.1
      sleep 1
      sample :elec_twang, amp: 0.2
      sleep 1
    end
  end
end

with_fx :bitcrusher, room: 0.7, mix: 0.7 do
  in_thread do
    sync :startDrumPhase1
    live_loop :drums, amp: 0.3 do
      sample :drum_tom_hi_hard, amp: 0.3
      sleep 1
      sample :drum_snare_soft, amp: 0.3
      sample :drum_cymbal_closed, amp: 0.3
      sleep 1
    end
  end
end

define :playKeys do |aKeySets, nAmp=0.5|
  aKeySets.each do |aKeySet|
    aKeySet.each do |sKeys|
      nGlobalSustain, nGlobalRelease, nGlobalAttack = nil
      aPlayKeys = []
      sKeys.split("|").each do |sKeyInfo|
        rSustain = sKeyInfo.match(/s[:=]([\d\.]+)/)
        rRelease = sKeyInfo.match(/r[:=]([\d\.]+)/)
        rAttack = sKeyInfo.match(/a[:=]([\d\.]+)/)
        rPause = sKeyInfo.match(/p[:=]([\d\.]+)/)
        nSustain = (rSustain and rSustain.length > 0) ? rSustain[1].to_f : nSustain
        nRelease = (rRelease and rRelease.length > 0) ? rRelease[1].to_f : nRelease
        nAttack = (rAttack and rAttack.length > 0) ? rAttack[1].to_f : nAttack
        nPause = (rPause and rPause.length > 0) ? rPause[1].to_f : nPause
        nGlobalSustain = nGlobalSustain.nil? ? nSustain : nil
        nGlobalRelease = nGlobalRelease.nil? ? nRelease : nil
        nGlobalAttack = nGlobalAttack.nil? ? nAttack : nil
        aPlayKeys << {
          :key => sKeyInfo.split(" ")[0],
          :sustain => nSustain,
          :release => nRelease,
          :attack => nAttack,
          :pause => nPause
        }
      end
      aPlayKeys.each do |rKey|
        nSustain = rKey[:sustain].nil? ? (nGlobalSustain.nil? ? 1 : nGlobalSustain) : rKey[:sustain]
        if nSustain.nil? then
          nSustain, nGlobalSustain = 1
        end
        sleep rKey[:pause].nil? ? 0 : rKey[:pause]
        play rKey[:key],
          sustain: nSustain,
          release: (rKey[:release].nil? ? (nGlobalRelease.nil? ? nSustain : nGlobalRelease) : rKey[:release]),
          attack: (rKey[:attack].nil? ? (nGlobalAttack.nil? ? 0 : nGlobalAttack) : rKey[:attack]),
          amp: nAmp
      end
      sleep nGlobalSustain.nil? ? 1 : nGlobalSustain
    end
  end
end