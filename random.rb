
use_bpm 50

in_thread do
  live_loop :amen_break do
    if one_in(3) then
      use_bpm 80 + rand*10
    else
      use_bpm 50
    end
    p = [0.125, 0.25, 0.5].choose
    with_fx :slicer, phase: p, wave: 0 do
      sample :loop_drone_g_97, beat_stretch: 2, rate: 1, amp: 2
    end
    with_fx :echo, phase: p*2, wave: 0 do
      sample :vinyl_rewind, beat_stretch: 1, amp: 1
    end
    sample :loop_weirdo, amp: 3
    sample :loop_tabla, amp: 3
    sleep 2
  end
end
