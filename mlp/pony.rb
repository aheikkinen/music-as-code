
bPlay=true
bKeyboard=true
bBeep=true
bDrums=true
bFlibble=true
bBeat=true
nKeyAmp=2.0
nDrumAmp=0.3
nFlibbleAmp=0.2
nBeeperAmp=0.2

with_fx :flanger, room: 0.7 do
  use_synth :pluck
  use_bpm 200
  
  aPart1 = [
    [:Gb5, 1.0, :D5, 1.0], [:A5, 1.0], [:Gb5, 1.0], [:E5, 2.0], [:A5, 3.0, :D5, 3.0]
  ]
  aPart2 = [
    [:D5, 1.0], [:E5, 1.0], [:D5, 1.0], [:Db5, 2.0], [:A4, 3.0, :D4, 3.0],
    [:B4, 2.0], [:D5, 1.0], [:Db5, 2.0], [:E5, 3.0], [:A4, 1.0], [:B4, 2.0], [:A4, 5.0],
  ]
  aPart3 = [
    [:B4, 2.0], [:Db5, 2.0], [:D5, 2.0], [:E5, 2.0],
    [:D5, 2.0], [:Db5, 1.0], [:D5, 1.0], [:E5, 1.0], [:D5, 4.0],
    [:Gb5, 0.5], [:Gb5, 0.5], [:Gb5, 0.5], [:Gb5, 1.0], [:D5, 0.5], [:E5, 1.0], [:D5, 1.0], [:Gb5, 1.0], [:E5, 1.0], [:D5, 2.0],
    [:Db5, 2.0], [:D5, 1.0], [:E5, 1.0], [:D5, 1.0],
    [:Gb5, 2.0, :D5, 2.0], [:A5, 1.0], [:A5, 1.0], [:Gb5, 1.0], [:E5, 1.0], [:Gb5, 1.0], [:E5, 1.0]
  ]
  aPartF = [
    [:D5, 10.0]
  ]

  in_thread do
      startBeeper
      playKeys [aPart1, aPart2], 2
      playKeys [aPart1], 1
      startDrums
      startFlibble
      playKeys [aPart3], 1
      bPlay = false
      sleep 2
      playKeys [aPartF], 1
      sleep 1
    end
end

in_thread do
  use_bpm 50
  while bPlay and bBeat do
      p = [0.125, 0.25, 0.5].choose
      with_fx :slicer, phase: p, wave: 0 do
        sample :loop_breakbeat, beat_stretch: 2, rate: 1, amp: 2
      end
      sleep 2
    end
  end
  
  define :playKeys do |aKeySets, nRepeat|
    nRepeat.times do
      for i in 0..aKeySets.length-1
        aKeys = aKeySets[i]
        for j in 0..aKeys.length-1
          play aKeys[j][0], sustain: aKeys[j][1], release: aKeys[j][1]*1.0, attack: 0.0, amp: nKeyAmp
          if aKeys[j][2] then
            play aKeys[j][2], sustain: aKeys[j][3], amp: nKeyAmp
          end
          sleep aKeys[j][1]
        end
      end
    end
  end
  
  define :startBeeper do
    in_thread do
      while bPlay and bBeep do
          sample :elec_pop, amp: nBeeperAmp
          sleep 1
          sample :elec_beep, amp: nBeeperAmp
          sleep 1
        end
      end
    end
    
  define :startDrums do
    in_thread do
      while bPlay and bDrums do
	  sample :drum_heavy_kick, amp: nDrumAmp
	  sleep 1
	  sample :drum_snare_hard, amp: nDrumAmp
	  sleep 1
	end
      end
    end
    
    define :startFlibble do
    in_thread do
      while bPlay and bFlibble do
	  sample :bd_haus, rate: 1, amp: nFlibbleAmp
	  sleep 0.5
	end
      end
    end
        