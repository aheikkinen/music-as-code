
aPart1 = ["Gb5|D5", "A5", "Gb5", "E5 s=2", "A5|D5 s=3"]
aPart2 = [
  "D5", "E5", "D5", "Db5 s=2", "A4|D4 s=3",
  "B4 s=2", "D5", "Db5 s=2", "E5 s=3", "A4", "B4 s=2", "A4 s=5",
]
aPart3 = [
  "B4 s=2", "Db5 s=2", "D5 s=2", "E5 s=2",
  "D5 s=2", "Db5", "D5", "E5", "D5 s=4",
  "Gb5 s=0.5", "Gb5 s=0.5", "Gb5 s=0.5", "Gb5", "D5 s=0.5", "E5", "D5", "Gb5", "E5", "D5 s=2",
  "Db5 s=2", "D5", "E5", "D5",
  "Gb5|D5 s=2", "A5", "A5", "Gb5", "E5", "Gb5", "E5"
]
aPart4 = ["D5 s=7"]

with_fx :flanger, room: 0.7 do
  use_bpm 200
  use_synth :pluck
  live_loop :base do
    playKeys [aPart1, aPart2]*2
    playKeys [aPart1]
    cue :drums, :flibble
    playKeys [aPart3]
    sleep 2
    playKeys [aPart4]
  end
end

in_thread do
  use_bpm 50
  live_loop :beat do
    p = [0.125, 0.25, 0.5].choose
    with_fx :slicer, phase: p, wave: 0 do
      sample :loop_breakbeat, beat_stretch: 2, rate: 1, amp: 2
    end
    sleep 2
  end
end

in_thread do
  use_bpm 200
  live_loop :beep do
    sample :elec_pop, amp: 0.2
    sleep 1
    sample :elec_beep, amp: 0.2
    sleep 1
  end
end

in_thread do
  sync :drums
  use_bpm 200
  live_loop :drums do
    sample :drum_heavy_kick, amp: 0.2
    sleep 1
    sample :drum_snare_hard, amp: 0.2
    sleep 1
  end
end

in_thread do
  sync :flibble
  use_bpm 200
  live_loop :flibble do
    sample :bd_haus, rate: 1, amp: 1
    sleep 0.5
  end
end

define :playKeys do |aKeySets, nAmp=1|
  aKeySets.each do |aKeySet|
    aKeySet.each do |sKeys|
      nGlobalSustain, nGlobalRelease, nGlobalAttack = nil
      aPlayKeys = []
      sKeys.split("|").each do |sKeyInfo|
        rSustain = sKeyInfo.match(/s[:=]([\d\.]+)/)
        rRelease = sKeyInfo.match(/r[:=]([\d\.]+)/)
        rAttack = sKeyInfo.match(/a[:=]([\d\.]+)/)
        rPause = sKeyInfo.match(/p[:=]([\d\.]+)/)
        nSustain = (rSustain and rSustain.length > 0) ? rSustain[1].to_f : nSustain
        nRelease = (rRelease and rRelease.length > 0) ? rRelease[1].to_f : nRelease
        nAttack = (rAttack and rAttack.length > 0) ? rAttack[1].to_f : nAttack
        nPause = (rPause and rPause.length > 0) ? rPause[1].to_f : nPause
        nGlobalSustain = nGlobalSustain.nil? ? nSustain : nil
        nGlobalRelease = nGlobalRelease.nil? ? nRelease : nil
        nGlobalAttack = nGlobalAttack.nil? ? nAttack : nil
        aPlayKeys << {
          :key => sKeyInfo.split(" ")[0],
          :sustain => nSustain,
          :release => nRelease,
          :attack => nAttack,
          :pause => nPause
        }
      end
      aPlayKeys.each do |rKey|
        nSustain = rKey[:sustain].nil? ? (nGlobalSustain.nil? ? 1 : nGlobalSustain) : rKey[:sustain]
        if nSustain.nil? then
          nSustain, nGlobalSustain = 1
        end
        sleep rKey[:pause].nil? ? 0 : rKey[:pause]
        play rKey[:key],
          sustain: nSustain,
          release: (rKey[:release].nil? ? (nGlobalRelease.nil? ? nSustain : nGlobalRelease) : rKey[:release]),
          attack: (rKey[:attack].nil? ? (nGlobalAttack.nil? ? 0 : nGlobalAttack) : rKey[:attack]),
          amp: nAmp
      end
      sleep nGlobalSustain.nil? ? 1 : nGlobalSustain
    end
  end
end