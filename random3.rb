with_fx :bitcrusher, room: 0.7, mix: 0.7 do
  use_bpm 320
  in_thread do
    live_loop :drums1, amp: 0.3 do
      4.times do
        sample :drum_tom_hi_hard, amp: 0.3
        sleep 1
        sample :drum_snare_soft, amp: 0.3
        sample :drum_cymbal_closed, amp: 0.3
        sleep 1
        sample :drum_heavy_kick
        sleep 1
      end
      cue :riff
      sample :drum_cymbal_closed, finish: 1
      cue :startDrums2
    end
  end
end

with_fx :bitcrusher, room: 0.7, mix: 0.7 do
  in_thread do
    sync :startDrums2
    live_loop :drums2, amp: 0.3 do
      sample :drum_tom_hi_hard, amp: 0.3
      sleep 1
      sample :drum_snare_soft, amp: 0.3
      sample :drum_cymbal_closed, amp: 0.3
      sleep 1
    end
  end
end

with_fx :bitcrusher, room: 0.7, mix: 0.7 do
  in_thread do
    sync :riff
    use_bpm 100
    live_loop :riff, amp: 0.3 do
      2.times do
        sample :guit_e_fifths, decay: 3
        sleep 5
      end
      sample :guit_em9, decay: 5
    end
  end
end