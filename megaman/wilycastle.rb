
bPlay=true
bBeat=true
bBeep=true
bDrums=true
nKeyAmp=1.0
nKeyAmp2=0.5

with_fx :bitcrusher, room: 0.5 do
  use_synth :subpulse
  use_bpm 300
  
  aPart1 = [
    [:E4, 0.5], [:E4, 0.5], [:E4, 1.0], [:E4, 0.5], [:E4, 0.5], [:E4, 1.0]
  ]
  aPart1b = [
    [:Eb4, 1.0], [:Ab5, 1.0], [:Ab5, 1.0], [:Gb5, 2.0], [:E5, 2.0], [:Eb5, 1.0], [:Db4, 2.0], [:Db4, 1.0],
    [:Ab5, 1.0], [:B5, 1.0], [:Bb5, 3.0], [:Db4, 2.0], [:Db4, 1.0], [:Ab5, 1.0], [:B5, 1.0], [:Bb5, 2.0]
  ]
  aPart1c = [
    [:Db4, 2.0], [:Db4, 1.0], [:Ab4, 1.0], [:B4, 1.0], [:Bb4, 2.0], [:Eb5, 1.0], [:E5, 1.0]
  ]
  aPart1d = [
    [:Db4, 2.0], [:Db4, 0.5], [:Db4, 0.5], [:E4, 1.0], [:E4, 0.5], [:E4, 0.5], [:E4, 1.0], [:Db4, 2.0],
    [:Ab4, 1.0], [:Gb4, 1.0], [:Ab4, 2.0]
  ]
  aPart1e = [
    [:Db4, 2.0], [:Ab4, 2.0], [:Gb4, 2.0], [:E4, 2.0], [:Gb4, 3.0], [:Gb4, 0.5], [:Gb4, 0.5], [:Gb4, 1.0], [:Gb4, 0.5], [:Gb4, 0.5], [:Gb4, 1.0],
    [:Eb4, 2.0], [:Ab5, 2.0], [:Gb5, 2.0], [:E5, 2.0], [:Eb5, 2.0], [:Db5, 2.0], [:Db4, 1.0], [:Ab5, 1.0], [:B5, 1.0], [:Bb5, 3.0],
    [:Db4, 2.0], [:Db4, 1.0], [:Ab4, 1.0], [:B4, 1.0], [:Bb4, 2.0], [:B4, 1.0], [:Db5, 4.0]
  ]
  aPart2 = [
    [:Db4, 2.0], [:Db4, 0.5], [:Db4, 0.5],
    [:E4, 1.0], [:E4, 0.5], [:E4, 0.5], [:E4, 1.0], [:Db4, 2.0],
    [:Ab4, 1.0], [:Gb4, 1.0], [:Ab4, 2.0]
  ]
  aPart3a = [
    [:Db4, 2.0], [:Ab4, 2.0], [:Gb4, 2.0], [:E4, 2.0], [:Gb4, 4.0]
  ]
  aPart3b = [
    [:Gb4, 0.5], [:Gb4, 0.5], [:Gb4, 1.0], [:Gb4, 0.5], [:Gb4, 0.5], [:Gb4, 1.0], [:Eb4, 2.0],
    [:Ab5, 2.0], [:Gb5, 2.0], [:E5, 2.0], [:Eb5, 2.0], [:Db5, 2.0],
    [:Db4, 1.0], [:Ab4, 1.0], [:B4, 1.0], [:Bb4, 4.0],
    [:Db4, 1.0], [:Db4, 1.0], [:Ab4, 1.0], [:B4, 1.0], [:Bb4, 2.0],
  ]
  aPart4 = [
    [:B4, 1.0], [:E5, 2.0], [:Db5, 2.0], [:B4, 2.0], [:Db5, 3.0], [:B4, 4.0], [:B4, 1.0], [:Db5, 3.0],
    [:Ab4, 1.0], [:A4, 1.0], [:Ab4, 1.0], [:E4, 2.0], [:E4, 1.0], [:Ab4, 1.0],  [:B4, 1.0], [:Db5, 5.0],
    [:B4, 1.0], [:E5, 2.0], [:Db5, 2.0], [:B4, 2.0], [:Db5, 2.0], [:B4, 4.0], [:B4, 1.0], [:Ab4, 1.0], [:B4, 1.0], 
    [:C5, 2.0], [:C5, 1.0], [:C5, 1.0], [:Eb5, 1.0], [:Ab5, 4.0]
  ]
  aPart5 = [
    [:B5, 1.0], [:Db6, 5.0], [:Db6, 2.0], [:B5, 1.0], [:Db6, 1.0], [:E6, 3.0], [:Ab6, 1.0], [:Gb6, 1.0], [:E6, 1.0], [:Eb6, 1.0],
    [:Db6, 5.0], [:Db6, 2.0], [:B5, 1.0], [:Db6, 1.0], [:E6, 3.0], [:E6, 1.0], [:Gb6, 1.0], [:E6, 2.0], [:Eb6, 3.0], [:Eb6, 1.0],
    [:Db6, 1.0], [:B5, 1.0], [:Ab6, 2.0], [:Ab6, 1.0], [:Gb6, 2.0], [:E6, 2.0], [:Eb6, 2.0], [:Eb6, 1.0], [:E6, 1.0], [:Eb6, 1.0],
    [:Db6, 6.0]
  ]
  aPart6 = [
    [:B5, 1.0], [:Db6, 4.0], [:B5, 1.0], [:E6, 2.0], [:Db6, 2.0], [:B5, 2.0], [:Db6, 2.0], [:B5, 3.0], [:B5, 1.0], [:Db6, 2.0],
    [:Ab5, 1.0], [:A5, 1.0], [:Ab5, 1.0], [:E5, 2.0], [:E5, 1.0], [:Ab5, 1.0], [:B5, 1.0], [:Db6, 4.0], [:B5, 1.0], [:E6, 2.0],
    [:Db6, 2.0], [:B5, 2.0], [:Db6, 2.0], [:B5, 4.0], [:B5, 1.0], [:Ab5, 1.0], [:B5, 1.0], [:C6, 2.0], [:C6, 1.0], [:C6, 1.0],
    [:Eb6, 1.0], [:Ab6, 2.0]
  ]
  aPart7 = [
    [:E5, 2.0], [:E5, 1.0], [:E5, 1.0], [:Eb5, 1.0], [:E5, 2.0], [:Db6, 1.0], [:B5, 2.0], [:A5, 2.0], [:Ab5, 2.0], [:Gb5, 2.0], [:Gb5, 2.0],
    [:Ab5, 1.0], [:Ab5, 2.0], [:Ab5, 3.0], [:Gb5, 2.0], [:Ab5, 1.0], [:Ab5, 2.0], [:Ab5, 2.0], [:Gb5, 1.0], [:E5, 2.0], [:E5, 1.0], [:E5, 1.0],
    [:Eb5, 1.0], [:E5, 2.0], [:Db6, 1.0], [:B5, 2.0], [:A5, 2.0], [:Ab5, 2.0], [:Gb5, 3.0], [:Eb5, 1.0], [:E5, 1.0], [:Eb5, 1.0], [:Db5, 6.0]
  ]
  aPart8 = [
    [:B4, 1.0], [:E5, 2.0], [:Db5, 2.0], [:B4, 2.0], [:Db5, 3.0], [:B4, 3.0], [:B4, 1.0], [:Db5, 3.0], [:Ab4, 1.0], [:A4, 1.0], [:Ab4, 1.0],
    [:E4, 2.0], [:E4, 1.0], [:AB4, 1.0], [:B4, 1.0], [:DB5, 4.0], [:B4, 1.0], [:E5, 2.0], [:Db5, 2.0], [:B4, 2.0], [:Db5, 2.0], [:B4, 3.0],
    [:B4, 1.0], [:AB4, 1.0], [:B4, 1.0], [:C5, 2.0], [:C5, 1.0], [:C5, 1.0], [:Eb5, 1.0], [:Ab5, 3.0]
  ]

  in_thread do
    sleep 1
    playKeys [aPart1, aPart2, aPart1], 1, nKeyAmp
    playKeys [aPart3a, aPart3b, [[:Eb5, 1.0, :B3, 1.0], [:E5, 1.0]]], 1, nKeyAmp
    playKeys [aPart1, aPart2, aPart1], 1, nKeyAmp
    playKeys [aPart3a, aPart3b, [[:B4, 1.0, :B3, 1.0], [:Db5, 6.0]]], 1, nKeyAmp
    playKeys [aPart4], 1, nKeyAmp
    playKeys [aPart1, aPart3a], 1, nKeyAmp
    playKeys [aPart1, aPart1b], 1, nKeyAmp
    playKeys [aPart5, aPart6, aPart7], 1, nKeyAmp
    playKeys [aPart1, aPart2, aPart1], 1, nKeyAmp
    playKeys [aPart3a, aPart3b, aPart1c], 1, nKeyAmp
    playKeys [aPart1, aPart1d], 1, nKeyAmp
    playKeys [aPart1, aPart1e], 1, nKeyAmp
    playKeys [aPart8], 1, nKeyAmp
    playKeys [aPart1, aPart1d], 1, nKeyAmp
    playKeys [aPart1, aPart3a], 1, nKeyAmp
    playKeys [aPart1, aPart1b], 1, nKeyAmp
    playKeys [aPart5, aPart6, aPart7], 1, nKeyAmp
    bPlay=false
  end
  
  in_thread do
    use_synth :pluck
    playKeys [[[:Db3, 1.0]]], 16, nKeyAmp2
    playKeys [[[:A3, 1.0]]], 16, nKeyAmp2
    playKeys [[[:B3, 1.0]]], 16, nKeyAmp2
    playKeys [[[:Db3, 1.0]]], 16, nKeyAmp2
    sleep 1
    playKeys [[[:Db3, 1.0]]], 16, nKeyAmp2
    playKeys [[[:A3, 1.0]]], 16, nKeyAmp2
    playKeys [[[:B3, 1.0]]], 16, nKeyAmp2
    playKeys [[[:Db3, 1.0]]], 16, nKeyAmp2
    startBeeper
    sleep 60
    startDrums
  end

  in_thread do
    use_bpm 150
    while bPlay and bBeat do
        p = [0.125, 0.25, 0.5].choose
        with_fx :slicer, phase: p, wave: 0 do
          sample :elec_twang, beat_stretch: 2, rate: 1, amp: 1
        end
        sleep 2
      end
    end
  end
  
  define :playKeys do |aKeySets, nRepeat, nAmp|
    nRepeat.times do
      for i in 0..aKeySets.length-1
        aKeys = aKeySets[i]
        for j in 0..aKeys.length-1
          play aKeys[j][0], sustain: aKeys[j][1], release: aKeys[j][1]*1.0, attack: 0.0, amp: nAmp
          if aKeys[j][2] then
            play aKeys[j][2], sustain: aKeys[j][3], amp: nAmp
          end
          sleep aKeys[j][1]
        end
      end
    end
  end

  define :startBeeper do
    in_thread do
      while bPlay and bBeep do
          sample :elec_pop, amp: 0.5
          sleep 1
          sample :elec_beep, amp: 0.5
          sleep 1
        end
      end
    end
  
  define :startDrums do
    in_thread do
      while bPlay and bDrums do
          sample :sn_zome, amp: 0.5
          sleep 1
          sample :sn_generic, amp: 0.5
          sleep 1
        end
      end
    end