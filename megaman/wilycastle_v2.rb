
aPart1 = ["E4|Db4 s=0.5", "E4|Db4 s=0.5", "E4|Db4"]
aPart1b = [
  "Eb4", "Ab5", "Ab5", "Gb5 s=2", "E5 s=2", "Eb5", "Db4 s=2", "Db4",
  "Ab5", "B5", "Bb5 s=3", "Db4 s=2", "Db4", "Ab5", "B5", "Bb5 s=2"
]
aPart1c = ["Db4 s=2", "Db4", "Ab4", "B4", "Bb4 s=2", "Eb5", "E5"]
aPart1d = [
  "Db4 s=2", "Db4 s=0.5", "Db4 s=0.5",
  "E4", "E4 s=0.5", "E4 s=0.5", "E4",
  "Db4 s=2", "Ab4", "Gb4", "Ab4 s=2"
]
aPart1e = [
  "Db4 s=2", "Ab4 s=2", "Gb4 s=2", "E4 s=2", "Gb4 s=3", "Gb4 s=0.5", "Gb4 s=0.5", "Gb4", "Gb4 s=0.5", "Gb4 s=0.5", "Gb4",
  "Eb4 s=2", "Ab5 s=2", "Gb5 s=2", "E5 s=2", "Eb5 s=2", "Db5 s=2", "Db4", "Ab5", "B5", "Bb5 s=3",
  "Db4 s=2", "Db4", "Ab4", "B4", "Bb4 s=2", "B4", "Db5 s=4"
]
aPart2 = [
  "Db4|Ab3 s=2", "Db4|Ab3 s=0.5", "Db4|Ab3 s=0.5",
  "E4|Db4", "E4|Db4 s=0.5", "E4|Db4 s=0.5", "E4|Db4",
  "Db4|Ab3 s=2", "E4|Ab3|Ab4", "B3|Eb4|Gb4", "E4|Ab3|Ab4 s=2"
]
aPart3a = [
  "Db4|Ab3 s=2", "E4|Db4|Ab4 s=2", "B3|Eb4|Gb4 s=2",
  "A3|E4|Db4 s=2", "B3|Eb4|Gb4 s=4"
]
aPart3b = [
  "Eb4|Gb4 s=0.5", "Eb4|Gb4 s=0.5", "Eb4|Gb4", "Eb4|Gb4 s=0.5", "Eb4|Gb4 s=0.5", "Eb4|Gb4",
  "B3|Eb4 s=2", "B3|E4|Ab5 s=2", "B3|Eb4|Gb5 s=2", "E5|Db4|Ab3 s=2",
  "B4|Gb4|Eb5 s=2", "Db5|Ab4 s=2",
  "Db4", "Ab4", "B4", "Bb4 s=4",
  "Db4 s=2", "Db4", "Ab4", "B4", "Bb4 s=2",
]
aPart3c = ["B3|Eb5", "E5|Db5 s=2"]
aPart3d = ["B4|B3", "Db5 s=6"]
aPart3e = [
  "Db4|Ab3 s=2", "Db4 s=0.5", "Db4 s=0.5",
  "E4", "E4 s=0.5", "E4 s=0.5", "E4",
  "Db4 s=2", "Ab4", "Gb4", "Ab4 s=2"
]
aPart3f = [
  "Db4|Ab3 s=2", "B3|Ab4 s=2", "A3|Gb4 s=2",
  "E4|Ab3 s=2", "A3|Gb4 s=4",
  "Gb4|E3 s=0.5", "Gb4|E3 s=0.5", "Gb4|E3",
  "Gb4|E3 s=0.5", "Gb4|E3 s=0.5", "Gb4|E3",
  "Eb4|Gb3", "Ab4|Ab3", "Ab4",
  "Gb4|Eb3 s=2", "E4|Eb3 s=2", "Eb4|Eb3", "Db4|Eb3 s=2",
  "Db4", "Ab4", "B4", "Bb4 s=3",
  "Db4 s=2", "Db4", "Ab4", "B4", "Bb4 s=2", "B4",
  "Db5 s=6"
]
aPart4 = [
  "B4", "E5 s=2", "Db5 s=2", "B4 s=2", "Db5 s=3", "B4 s=4", "B4", "Db5 s=3",
  "Ab4", "A4", "Ab4", "E4 s=2", "E4", "Ab4", "B4", "Db5 s=5",
  "B4", "E5 s=2", "Db5 s=2", "B4 s=2", "Db5 s=2", "B4 s=4", "B4", "Ab4", "B4",
  "C5 s=2", "C5", "C5", "Eb5", "Ab5 s=4"
]
aPart5= [
  "Db5 s=2", "B4", "Db5", "E5 s=3", "Ab5", "Gb5", "E5", "Eb5",
  "Db5 s=5", "Db5 s=2", "B4", "Db5", "E5 s=3", "E5", "Gb5", "E5 s=2", "Eb5 s=3", "Eb5",
  "Db5", "B4", "Ab5 s=2", "Ab5", "Gb5 s=2", "E5 s=2", "Eb5 s=2", "Eb5", "E5", "Eb5",
  "Db5 s=6"
]
aPart6 = [
  "B4", "Db5 s=4", "B4", "E5 s=2", "Db5 s=2", "B4 s=2", "Db5 s=2", "B4 s=3", "B4", "Db5 s=2",
  "Ab4", "A4", "Ab4", "E4 s=2", "E4", "Ab4", "B4", "Db5 s=4", "B4", "E5 s=2",
  "Db5 s=2", "B4 s=2", "Db5 s=2", "B4 s=4", "B4", "Ab4", "B4", "C5 s=2", "C5", "C5",
  "Eb5", "Ab5 s=2"
]
aPart7 = [
  "E4 s=2", "E4", "E4", "Eb4", "E4 s=2", "Db5", "B4 s=2", "A4 s=2", "Ab4 s=2", "Gb4 s=2", "Gb4 s=2",
  "Ab4", "Ab4 s=2", "Ab4 s=3", "Gb4 s=2", "Ab4", "Ab4 s=2", "Ab4 s=2", "Gb4", "E4 s=2", "E4", "E4",
  "Eb4", "E4 s=2", "Db5", "B4 s=2", "A4 s=2", "Ab4 s=2", "Gb4 s=3", "Eb4", "E4", "Eb4", "Db4 s=6"
]
aPart8 = [
  "B4", "E5 s=2", "Db5 s=2", "B4 s=2", "Db5 s=3", "B4 s=3", "B4", "Db5 s=3", "Ab4", "A4", "Ab4",
  "E4 s=2", "E4", "AB4", "B4", "DB5 s=4", "B4", "E5 s=2", "Db5 s=2", "B4 s=2", "Db5 s=2", "B4 s=3",
  "B4", "AB4", "B4", "C5 s=2", "C5", "C5", "Eb5", "Ab5 s=3"
]

with_fx :rbpf, amp: 1, room: 0.7, mix: 0.7 do
  use_bpm 320
  live_loop :base do
    use_synth :prophet
    
    playKeys [aPart1*2, aPart2, aPart1*2]
    playKeys [aPart3a, aPart3b, aPart3c]
    cue :elec
    
    playKeys [aPart1*2, aPart2, aPart1*2]
    playKeys [aPart3a, aPart3b, aPart3d]
    playKeys [aPart4]
    playKeys [aPart1*2, aPart3e]
    playKeys [aPart1*2, aPart3f]
    
    # CONT HERE: 50sek https://www.youtube.com/watch?v=USvCs-ce8LE
    playKeys [aPart5, aPart6, aPart7]
    
    playKeys [aPart1*2, aPart2, aPart1*2]
    playKeys [aPart3a, aPart3b, aPart1c]
    playKeys [aPart1*2, aPart1d]
    playKeys [aPart1*2, aPart1e]
    playKeys [aPart8]
    playKeys [aPart1*2, aPart1d]
    playKeys [aPart1*2, aPart3a]
    playKeys [aPart1*2, aPart1b]
    playKeys [aPart5, aPart6, aPart7]
  end
end

in_thread do
  sync :elec
  live_loop :background do
    use_bpm 320
    sample :elec_pop, amp: 2
    sleep 1
    sample :elec_beep, amp: 2
    sleep 1
  end
end

in_thread do
  #sync :drums
  live_loop :drums do
    use_bpm 320
    sample :glitch_bass_g, amp: 1
    sleep 1
    sample :glitch_perc5, amp: 1
    sleep 1
  end
end

define :playKeys do |aKeySets, nAmp=1|
  aKeySets.each do |aKeySet|
    aKeySet.each do |sKeys|
      nGlobalSustain, nGlobalRelease, nGlobalAttack = nil
      aPlayKeys = []
      sKeys.split("|").each do |sKeyInfo|
        rSustain = sKeyInfo.match(/s[:=]([\d\.]+)/)
        rRelease = sKeyInfo.match(/r[:=]([\d\.]+)/)
        rAttack = sKeyInfo.match(/a[:=]([\d\.]+)/)
        rPause = sKeyInfo.match(/p[:=]([\d\.]+)/)
        nSustain = (rSustain and rSustain.length > 0) ? rSustain[1].to_f : nSustain
        nRelease = (rRelease and rRelease.length > 0) ? rRelease[1].to_f : nRelease
        nAttack = (rAttack and rAttack.length > 0) ? rAttack[1].to_f : nAttack
        nPause = (rPause and rPause.length > 0) ? rPause[1].to_f : nPause
        nGlobalSustain = nGlobalSustain.nil? ? nSustain : nil
        nGlobalRelease = nGlobalRelease.nil? ? nRelease : nil
        nGlobalAttack = nGlobalAttack.nil? ? nAttack : nil
        aPlayKeys << {
          :key => sKeyInfo.split(" ")[0],
          :sustain => nSustain,
          :release => nRelease,
          :attack => nAttack,
          :pause => nPause
        }
      end
      aPlayKeys.each do |rKey|
        nSustain = rKey[:sustain].nil? ? (nGlobalSustain.nil? ? 1 : nGlobalSustain) : rKey[:sustain]
        if nSustain.nil? then
          nSustain, nGlobalSustain = 1
        end
        sleep rKey[:pause].nil? ? 0 : rKey[:pause]
        play rKey[:key],
          sustain: nSustain,
          release: (rKey[:release].nil? ? (nGlobalRelease.nil? ? nSustain : nGlobalRelease) : rKey[:release]),
          attack: (rKey[:attack].nil? ? (nGlobalAttack.nil? ? 0 : nGlobalAttack) : rKey[:attack]),
          amp: nAmp
      end
      sleep nGlobalSustain.nil? ? 1 : nGlobalSustain
    end
  end
end